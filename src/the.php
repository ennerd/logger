<?php
namespace The;

/**
 * Must return a PSR-3 compliant logger. Should discover if other frameworks are loaded and return their
 * logger instance before returning the default logger instance.
 *
 * Even better is if other frameworks provide access to their logger instance through \The\logger() directly,
 * then marks that their framework provides The\Logger in composer.
 *
 * @return Psr\Log\LoggerInterface
 */
function logger() {
	return Logger::getInstance();
}

/**
 * Must alias the Logger class to The\Logger.
 */
\The::setDefaultClass( Logger::class, Logger\Logger::class );
