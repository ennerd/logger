# The\Logger

A simple way to access the logging service in a project. If no logging service is available, logs to STDERR.

## Usage

To access the system default logger, simply do this:

```php
\The\logger()->emergency('This is an emergency! Calling Doctor {name}', ['name' => 'Wolf']);
```

## Interoperability

I accept pull requests if you want to provide interoperability with other frameworks. There are two ways a 
framework can be supported:

### Auto Detection

The src/the.php file has a function named "logger". This function should detect if we are operating in
a framework such as Symfony, Laravel or Zend - and return the appropriate logger.

### Composer Provides

The framework itself can provide the The\Logger by declaring it in the composer.json file:

```json
{
    "provide": [ 'the/logger' ]
}
```

and then declaring a function in the The namespace like this

```php
namespace The {
    function logger() {
        /* return PSR-3 logger instance */
    }
}
```

