<?php
namespace The\Logger;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;

class Logger implements LoggerInterface {
	use LoggerTrait;

	public static function getInstance() {
		static $instance;
		if($instance) return $instance;
		return $instance = new static();
	}

	public function log($level, $message, array $context = array()) {
		$interpolate = function($message, array $context) {
			$replace = array();
			foreach ($context as $key => $val) {
				// check that the value can be casted to string
				if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
					$replace['{' . $key . '}'] = $val;
				}
			}
			// interpolate replacement values into the message and return
			return strtr($message, $replace);
		};

		fwrite(STDERR, gmdate('Y-m-d H:i:s').' ('.$level.'): '.$interpolate($message, $context)."\n");
	}
}
